const http = require('http');
const express = require('express');
const consola = require('consola');
const passport = require('passport');
const cookieParser = require('cookie-parser');
const socketIO = require('socket.io');
const swaggerUi = require('swagger-ui-express');
const docs = require('./docs');
const { ApiRes } = require('~core/base/utils');

// Mongo
require('./config/db')

// Server
const app = express();
const server = http.createServer(app)
const io = socketIO(server);

// Express
app.use(cookieParser());
app.use(express.json());

// CORS
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, PATCH, DELETE')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next();
});

// Passport
app.use(passport.initialize());
app.use(passport.session());

// Auth
app.use(require('~core/auth/token').useToken);

// Docs
app.use('/docs', swaggerUi.serve, swaggerUi.setup(docs));

// Routers
app.use(require('~core/base/router'));
app.use(require('~core/auth/router'));
app.use(require('~core/events/router'));
app.use(require('~core/users/router'));

// Errors
app.use (function (error, req, res, next){
    const apiRes = new ApiRes(res);

    if (error && error.type === 'entity.parse.failed') {
        return apiRes.sendBadModel({ message: 'Invalid json!'})
    }

    next(error)
});

// Start
const port = process.env.PORT || 3005;
server.listen(port);
consola.ready({
    message: `Server listening on port ${port}`,
    badge: true
});

