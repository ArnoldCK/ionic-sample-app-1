const fs = require('fs');
const confirmEmailTpl = fs.readFileSync('./email/confirm.html', 'utf8');
const logoText = 'Cryptopoly';
const logoUrl = '';

function confirmEmailHtml(
    {
        title = 'Confirm Email',
        ctaUrl = '',
        ctaText = 'Confirm Your Email',
        sender = 'Cryptopoly.com',
        senderUrl = 'Cryptopoly.com'
    } = {}) {

    let tpl = confirmEmailTpl;
    const opts = {title, ctaUrl, ctaText, sender, senderUrl, logoText, logoUrl};
    Object.keys(opts).forEach(k => {
        const rgx = new RegExp(`{{${k}}}`, 'g');
        tpl = tpl.replace(rgx, opts[k])
    })
    return tpl;
}

function resetPasswordHtml(
    {
        title = 'Reset Password',
        ctaUrl = '',
        ctaText = 'Reset PAssword',
        sender = 'Cryptopoly.com',
        senderUrl = 'Cryptopoly.com'
    } = {}) {

    let tpl = confirmEmailTpl;
    const opts = {title, ctaUrl, ctaText, sender, senderUrl, logoText, logoUrl};
    Object.keys(opts).forEach(k => {
        const rgx = new RegExp(`{{${k}}}`, 'g');
        tpl = tpl.replace(rgx, opts[k])
    })
    return tpl;
}

function welcomeHtml(
    {
        title = 'Reset Password',
        ctaUrl = '',
        ctaText = 'Confirm Your Email',
        sender = 'Cryptopoly.com',
        senderUrl = 'Cryptopoly.com'
    } = {}) {

    let tpl = confirmEmailTpl;
    const opts = {title, ctaUrl, ctaText, sender, senderUrl, logoText, logoUrl};
    Object.keys(opts).forEach(k => {
        const rgx = new RegExp(`{{${k}}}`, 'g');
        tpl = tpl.replace(rgx, opts[k])
    })
    return tpl;
}

module.exports = {
    confirmEmailHtml,
    resetPasswordHtml,
    welcomeHtml
}