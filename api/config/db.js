import opts from './opts';
import mongoose from 'mongoose';

mongoose.connect(opts.mongo.url, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
}, error => {
    if (error) return console.log('MongoDB Error : ', error.name)

    console.log(`Mongodb connected successfully at ${opts.mongo.url}`)
})
