const isDev = process.env.NODE_ENV !== 'production';
const port = process.env.PORT || 3005;
const host = isDev ? `http://localhost:${port}/` : process.env.HOST;
const clientHost = isDev ? `http://localhost:3000/`: 'https://cryptopoly.com/'
const { hmsToMsecs } = require('~shared/utils');

module.exports = {
    baseRoute: '/',
    host,
    clientHost,
    mongo: {
        url: `${process.env.DB_URL}${process.env.DB_NAME}`
    },
    verification: {
        maxTries: 3,
        maxResets: 3,
        trialExpiry: isDev ? hmsToMsecs('5m') : hmsToMsecs('2h'),
        lockedExpiry: isDev ? hmsToMsecs('5m') : hmsToMsecs('2h'),
    },
    support: {
        email: process.env.SUPPORT_EMAIL,
        emailPass: process.env.SUPPORT_EMAIL_PASS
    },
    passport: {
        google: {
            clientID: process.env.GOOGLE_ID,
            clientSecret: process.env.GOOGLE_SECRET,
            callbackURL: `${host}auth/google/callback`
        },
        paths: {
            google: '/auth/google',
            facebook: '/auth/facebook'
        },
        keys: {
            jwt: process.env.JWT_KEY,
            cookie: process.env.COOKIE_KEY
        },
        jwt: {
            expiresIn: '7d'
        },
        clientRedirect: {
            success: `${process.env.CLIENT_HOST}auth/login/success`,
            error: `${process.env.CLIENT_HOST}auth/login/error`
        }
    }
}
