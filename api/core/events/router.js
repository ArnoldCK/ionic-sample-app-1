const express = require('express');
const { memberOnly } = require('~core/auth/guard');
const { validateModel, validateParams } = require('~core/base/utils');
const { addEvent, updateEvent, delEvent, getEvent, getEvents } = require('./controller');
const router = new express.Router();
const route = '/events/';

/**
 * @swagger
 * definitions:
 *   Event:
 *     properties:
 *       _id:
 *         type: string
 *       comment:
 *         type: string
 *       time:
 *         type: string
 *       userId:
 *         type: string
 */

/**
 * @swagger
 * /events/:
 *    get:
 *      tags:
 *        - Events
 *      description: Get all events
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: ''
 *          schema:
 *            type: object
 *            properties:
 *              success:
 *                type: boolean
 *              status:
 *                type: number
 *              data:
 *                type: array
 *                items:
 *                  $ref: "#/definitions/Event"
 */
router.get(route, getEvents)

/**
 * @swagger
 * /events/{id}:
 *    get:
 *      tags:
 *        - Events
 *      description: Get single event
 *      parameters:
 *        - name: id
 *          required: true
 *          in: path
 *          type: string
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: ''
 *          schema:
 *            type: object
 *            properties:
 *              success:
 *                type: boolean
 *              status:
 *                type: number
 *              data:
 *                type: object
 *                $ref: "#/definitions/Event"
 */
router.get(`${route}:id`, validateParams(['id*=mongoId']), getEvent)

/**
 * @swagger
 * /events/:
 *    post:
 *      tags:
 *        - Events
  *      security:
 *        - Bearer: []
 *      description: Post new event
 *      parameters:
 *        - name: body
 *          in: body
 *          required: true
 *          description: Event object
 *          schema:
 *            $ref: "#/definitions/Event"
 *      produces:
 *        - application/json
 *      responses:
 *        201:
 *          description: Created
 *          schema:
 *            type: object
 *            properties:
 *              success:
 *                type: boolean
 *              status:
 *                type: number
 *              data:
 *                type: object
 *                $ref: "#/definitions/Event"
 */
router.post(route, memberOnly, validateModel(['comment*', 'time*', 'userId*']), addEvent);

/**
 * @swagger
 * /events/{id}:
 *    put:
 *      tags:
 *        - Events
 *      security:
 *        - Bearer: []
 *      description: Update event
 *      parameters:
 *        - name: id
 *          in: path
 *          required: true
 *        - name: body
 *          in: body
 *          required: true
 *          description: Event object
 *          schema:
 *            $ref: "#/definitions/Event"
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Updated
 *          schema:
 *            type: object
 *            properties:
 *              success:
 *                type: boolean
 *              status:
 *                type: number
 *              data:
 *                type: object
 *                $ref: "#/definitions/Event"
 */
router.put(route, memberOnly, validateModel(['_id*', 'comment', 'time', 'userId']), updateEvent)

/**
 * @swagger
 * /events/{id}:
 *    delete:
 *      tags:
 *        - Events
 *      security:
 *        - Bearer: []
 *      description: Delete event by id
 *      parameters:
 *        - name: id
 *          in: path
 *          required: true
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Removed
 *          schema:
 *            type: object
 *            properties:
 *              success:
 *                type: boolean
 *              status:
 *                type: number
 *              data:
 *                type: object
 *                $ref: "#/definitions/Event"
 */
router.delete(`${route}:id`, validateParams(['id*=mongoId']), delEvent)

module.exports = router
