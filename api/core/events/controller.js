const Event = require('./model');
const { ApiRes } = require('~core/base/utils');

module.exports = {
    async addEvent(req, res) {
        const apiRes = new ApiRes(res);

        try {
            const event = new Event(req.body)
            const data = await event.save();
            apiRes.sendSuccess({ data });
        } catch (error) {
            apiRes.sendError({ error })
        }
    },

    async updateEvent(req, res) {
        const apiRes = new ApiRes(res);

        try {
            const data = await Event.findByIdAndUpdate(req.body._id, req.body, { new: true })
            if (!data) return apiRes.sendNotFound();

            apiRes.sendSuccess({ data });
        } catch (error) {
            apiRes.sendError({ error })
        }
    },
    
    async delEvent(req, res) {
        const apiRes = new ApiRes(res);

        try {
            const data = await Event.findByIdAndRemove(req.params.id)
            if (!data) return apiRes.sendNotFound();

            apiRes.sendSuccess({ data });
        } catch (error) {
            apiRes.sendError({ error })
        }
    },

    async getEvent(req, res) {
        const apiRes = new ApiRes(res);

        try {
            const data = await Event.findById(req.params.id);
            if (!data) return apiRes.sendNotFound();

            apiRes.sendSuccess({ data });
        } catch (error) {
            apiRes.sendError({ error })
        }
    },

    async getEvents(req, res) {
        const apiRes = new ApiRes(res);

        try {
            const data = await Event.find();
            if (!data || !data.length) return apiRes.sendEmpty({ data: [] });

            return apiRes.sendSuccess({ data });
        } catch (error) {
            apiRes.sendError({ error })
        }
    },
}