const mongoose = require('mongoose');

const schema = mongoose.Schema({
    comment: {
        type: String,
        trim: true,
        required: true
    },
    time: {
        type: Date,
        trim: true,
        required: true
    },
    userId: {
        type: String,
        required: true
    }
})

schema.set('toObject', {
    transform: function (doc, ret) {
      delete ret.__v
    }
})

schema.set('toJSON', {
    transform: function (doc, ret) {
      delete ret.__v
    }
})

const Event = mongoose.model('Event', schema)
module.exports = Event
