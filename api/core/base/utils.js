const { Validator } = require('node-input-validator');
const { ApiRes } = require('./types');

function modelQuery(keys, query) {
    let q = {}

    keys.forEach(k => {
        const v = query[k];
        if (v) q[k] = v
    })

    return q;
}

async function validateInput(type, model, input, req, res, next) {
    const apiRes = new ApiRes(res);

    if (!model.length) return apiRes.sendBadModel({ message: 'Validation rules missing!' })

    const keys = Object.keys(input);
    const onlyKeys = model.map(k => k.split('=')[0]);
    const allowedKeys = onlyKeys.map(k => k.replace('*', ''));
    const foreignKeysError = `Only these field(s) are allowed: [ ${allowedKeys.join(', ')} ]`;

    const foreignKeys = keys.filter(k=> !allowedKeys.includes(k));
    if (foreignKeys.length) return apiRes.sendBadModel({ message : `Foreign field(s) [ ${foreignKeys.join(', ')} ] are not allowed. ${foreignKeysError}` });

    const rules = {};

    model.forEach(k => {
        const parts = k.split('=');
        const key = parts[0].replace('*', '');
        const required = parts[0].includes('*');

        if (required) rules[key] = 'required';
        if (parts.length === 2) rules[key] = parts[1];
        if (parts.length === 2 && required) rules[key] = 'required|' + parts[1];
    })

    const v = new Validator(input, rules)

    const pass = await v.check();

    if (pass) return next();

    return apiRes.sendBadModel({ status: 422, errors: v.errors, message: type === 'query' ? 'Invalid query!' : 'Invalid model!' });
}

function validateModel(model = []) {
    return async function (req, res, next) {
        return validateInput('body', model, req.body, req, res, next)
    }
}

function validateQuery(model = []) {
    return async function (req, res, next) {
        return validateInput('query', model, req.query, req, res, next)
    }
}

function validateParams(model = []) {
    return async function (req, res, next) {
        return validateInput('params', model, req.params, req, res, next)
    }   
}

module.exports = {
    ApiRes,
    modelQuery,
    validateQuery,
    validateModel,
    validateParams
}