const { isString, isObject } = require('~shared/utils');

const apiError = {
    generic( { error, name, message } = {}) {
        name =  name || isObject(error) && error.name || 'generic';

        return {
            name,
            message: getErrorMessage(error, message)
        }
    },

    badModel(errors, message = 'Invalid Model') {
        return {
            name: 'badModel',
            message,
            messages: listErrorMessages(errors),
            errors
        }
    },

    badQuery(errors, message = 'Invalid query') {
        return {
            name: 'badQuery',
            message,
            messages: listErrorMessages(errors),
            errors
        }
    },

    badId() {
        return {
            name: 'badId',
            message: 'Invalid object id! Try again.'
        }       
    },

    tokenMissing({ status }) {
        return {
            status,
            name: 'tokenMissing',
            message: 'Bearer authorization token missing!'
        }       
    },

    tokenExpired({ status }) {
        return {
            status,
            name: 'tokenExpired',
            message: 'Authorization token expired or missing!. Please log in to continue.'
        }       
    },

    accessDenied() {
        return {
            name: 'accessDenied',
            message: 'Access denied! Try using an elevated account.'
        }       
    },

    notFound(message = 'Not found.') {
        return {
            name: 'notFound',
            message
        }       
    },

    empty(message = 'Empty results') {
        return {
            name: 'empty',
            message
        }       
    }
}

// #region Utils
function listErrorMessages(errors) {
    if (!errors) return [];
    let messages = [];
    for (let k in errors) {
        messages.push(errors[k].message)
    }

    return messages
}

function getErrorMessage(error, fallback = 'Error ocurred. No further details were provided. Contact support.') {
    return isString(error) && error || isObject(error) && error.message || fallback
}
// #endregion

module.exports = {
    apiError
}