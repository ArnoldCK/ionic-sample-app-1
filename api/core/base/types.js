const { apiError } = require('./errors');

class ApiRes {
    constructor(res) {
        this.res = res
    }

    sendSuccess({ status = 200, data, message } = {}) {
        return this.res.status(status).send({
            success: true,
            status,
            data,
            message
        })
    }

    sendCreated({ status = 201, data =null, message = 'created' } = {}) {
        return this.res.status(status).send({
            success: true,
            status,
            data,
            message
        })
    }

    sendError({status = 400, error = null, message, name } = {}) {
        this.res.hasError = true;

        return this.res.status(status).send({
            success: false,
            status,
            error: apiError.generic({ error, name, message })
        })
    }

    sendNoToken({status = 401} = {}) {
        this.res.hasError = true;
        return this.res.status(status).send({
            success: false,
            status,
            error: apiError.tokenMissing({ status })
        })
    }

    sendBadModel({status = 400, message = 'Invalid model! Try again.', errors = null } = {}) {
        this.res.hasError = true;

        return this.res.status(status).send({
            success: false,
            status,
            error: apiError.badModel(errors, message)
        })
    }

    sendBadQuery({status = 400, message = 'Invalid query! Try again.', errors = null} = {}) {
        this.res.hasError = true;

        return this.res.status(status).send({
            success: false,
            status,
            error: apiError.badQuery(errors, message)
        })
    }

    sendBadId({status = 400 } = {}) {
        this.res.hasError = true;

        return this.res.status(status).send({
            success: false,
            status,
            error: apiError.badId()
        })
    }

    sendExpired({status = 401 } = {}) {
        this.res.hasError = true;

        return this.res.status(status).send({
            success: false,
            status,
            error: apiError.tokenExpired({ status })
        })
    }

    sendDenied({status = 400} = {}) {
        this.res.hasError = true;
        return this.res.status(status).send({
            success: false,
            status,
            error: apiError.accessDenied()
        })
    }

    sendNotFound({status = 404, message = null } = {}) {
        return this.res.status(status).send({
            success: false,
            status,
            error: apiError.notFound(message)
        })
    }

    sendEmpty({status = 200, message, data } = {}) {
        return this.res.status(status).send({
            success: false,
            status,
            data,
            message
        })
    }
}

module.exports = {
    ApiRes
}
