const express = require('express');
const { ApiRes } = require('./utils');
const route = '/';
const router = new express.Router();

/**
 * @swagger
 * definitions:
 *   ApiRes:
 *     properties:
 *       success:
 *         type: boolean
 *       status:
 *         type: number
 *       data:
 *         type: object
 *       error:
 *         type: object
 *       message:
 *         type: string
 */

/**
 * @swagger
 * /:
 *    get:
 *      tags:
 *        - Base
 *      description: This should return api status
 *      responses:
 *        200:
 *          description: ''
 *          schema:
 *            type: object
 *            $ref: "#/definitions/ApiRes"
 */
router.get(route, (req, res) => {
    const apiRes = new ApiRes(res);
    return apiRes.sendSuccess({ message : 'All systems working!' })
})

module.exports = router
