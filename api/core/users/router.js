const express = require('express');
const router = new express.Router();
const { ApiRes } = require('~core/base/utils');
const route = '/users/';
const users = [
    {
        id: '001',
        firstName: 'Chris',
        lastName: 'Evans'
    },
    {
        id: '002',
        firstName: 'Robert',
        lastName: 'Downey'
    },  
    {
        id: '003',
        firstName: 'Scarlet',
        lastName: 'Johanson'
    },
    {
        id: '004',
        firstName: 'Mark',
        lastName: 'Ruffalo'
    }, 
    {
        id: '005',
        firstName: 'Chris',
        lastName: 'Evans'
    },
    {
        id: '006',
        firstName: 'Samuel',
        lastName: 'Jackson'
    }, 
]

/**
 * @swagger
 * definitions:
 *   DummyUser:
 *     properties:
 *       firstName:
 *         type: string
 *       lastName:
 *         type: string
 *       id:
 *         type: string
 */

/**
 * @swagger
 * /users/:
 *    get:
 *      tags:
 *        - Users
 *      description: Get all dummy users
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: ''
 *          schema:
 *            type: object
 *            properties:
 *              success:
 *                type: boolean
 *              status:
 *                type: number
 *              data:
 *                type: array
 *                items:
 *                  $ref: "#/definitions/DummyUser"
 */
router.get(route, (req, res) => {
    const apiRes = new ApiRes(res);
    apiRes.sendSuccess({ data: users })
})

module.exports = router;