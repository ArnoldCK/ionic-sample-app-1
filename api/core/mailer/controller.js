const nodemailer = require('nodemailer');
const opts = require('~config/opts');
const { ApiRes, validateModel } = require('~core/base/utils');
const { confirmEmailHtml } = require('~email');

function validateMail(req, res, next) {
    const required = ['to', 'from', 'subject', 'html'];
    const allowed = required;

    return validateModel({ 
        required, 
        allowed
    }, false)(req, res, next)
}

const transporter = nodemailer.createTransport({
    service: 'gmail',
    host: 'smtp.gmail.com',
    secureConnection: false,
    port: 25,
    auth: {
        user: opts.support.email,
        pass: opts.support.emailPass
    },
    tls: {
        rejectUnauthorized: false
    }
});

function trySendMail(mail) {
    return new Promise((resolve, reject) => {
        transporter.sendMail(mail, (error, info) => {
        if (error) return reject(error)
        resolve(info)
      });    
    })
}

module.exports = {
    async sendMail(req, res, next) {
        const apiRes = new ApiRes(res);
        const error = validateMail(req, res, next)
        if (error) return apiRes.sendError({ error });

        try {
            const data = await trySendMail(req.body);
            apiRes.sendSuccess({ data });
        } catch (error) {
            return apiRes.sendError({ error })
        }
    },

    async sendConfirmationEmail(code, to) {
        const html = confirmEmailHtml({ ctaUrl: `${opts.clientHost}auth/verifyemail?c=${code}` })
        const mail = {
            to,
            html,
            from: opts.support.email,
            subject: 'Please Confirm Email',
        }

        return new Promise((resolve, reject) => {
            transporter.sendMail(mail, (error, info) => {
                if (error) return reject(error)
                resolve(info)
            })
        })
    },

    async sendPasswordResetEmail(code, to) {
        const html = confirmEmailHtml({ title: 'Reset Your Password', ctaText: 'Reset Password', ctaUrl: `${opts.clientHost}auth/resetpassword?e=${to}&c=${code}` })
        const mail = {
            to,
            html,
            from: opts.support.email,
            subject: 'Password Reset',
        }

        return new Promise((resolve, reject) => {
            transporter.sendMail(mail, (error, info) => {
                if (error) return reject(error)
                resolve(info)
            })
        })
    },

    trySendMail,
    transporter
}
