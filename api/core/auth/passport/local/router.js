const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('~core/auth/user/model');
const { registerUser, recoverByEmail, resendEmail, resetPassword } = require('./controller');
const { ApiRes, validateModel } = require('~core/base/utils');
const route = '/auth/local/';

/**
 * @swagger
 * definitions:
 *   NewUser:
 *     properties:
 *       username:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *   LoginUser:
 *     properties:
 *       email:
 *         type: string
 *       password:
 *         type: string
 */

passport.use(new LocalStrategy({ usernameField: 'email', passwordField: 'password'}, async (email, password, done) => {
    try {
        const user = await User.findOne({ email });

        if (!user || !user.authenticate(password)) throw ('Wrong username or password!');

        // if (!user.emailVerification.passed) throw ({ message: 'Email not verified', name: 'emailNotVerified' })

        const token = await user.setToken();
        return done(null, token);
    } catch (e) {
        return done(e);
    }
}));

module.exports = function(router) {
    /**
     * @swagger
     * /auth/local/login:
     *    post:
     *      tags:
     *        - Auth
     *      description: Login using email and password
     *      parameters:
     *        - name: body
     *          in: body
     *          required: true
     *          description: New user object
     *          schema:
     *            $ref: "#/definitions/LoginUser"
     *      produces:
     *        - application/json
     *      responses:
     *        200:
     *          description: Returns token in data property
     *          schema:
     *            type: object
     *            properties:
     *              success:
     *                type: boolean
     *              status:
     *                type: number
     *              data:
     *                type: string
     */
    router.post(
        `${route}login`, 
        validateModel(['email*=email', 'password*']),
        passport.authenticate('local', { session: false, failWithError: true }),

        (req, res) => {
            const apiRes = new ApiRes(res);
            return apiRes.sendSuccess({ data: req.user });
        },

        (error, req, res, next) => {
            const apiRes = new ApiRes(res);
            return apiRes.sendError({ error })
        }
    )

    /**
     * @swagger
     * /auth/local/register:
     *    post:
     *      tags:
     *        - Auth
     *      description: Register using email and password
     *      parameters:
     *        - name: body
     *          in: body
     *          required: true
     *          description: New user object
     *          schema:
     *            $ref: "#/definitions/NewUser"
     *      produces:
     *        - application/json
     *      responses:
     *        200:
     *          description: Updated
     *          schema:
     *            type: object
     *            properties:
     *              success:
     *                type: boolean
     *              status:
     *                type: number
     *              data:
     *                type: object
     *                $ref: "#/definitions/NewUser"
     */
    router.post( 
        `${route}register`,
        validateModel(['email*=email', 'password*', 'username*']), 
        registerUser
    )

    router.post(
        `${route}resendemail`, 
        validateModel(['email*=email']), 
        resendEmail
    )

    router.put(
        `${route}resetpassword`,
        validateModel(['email*=email', 'password*', 'code*']),
        resetPassword
    )

    router.post(
        `${route}recoverbyemail`,
        validateModel( ['email*=email']),
        recoverByEmail
    )
}