const User = require('~core/auth/user/model');
const { sendConfirmationEmail, sendPasswordResetEmail } = require('~core/mailer/controller');
const { initRole } = require('~core/auth/utils');
const { ApiRes } = require('~core/base/utils');

async function registerUser(req, res, next) {
    const apiRes = new ApiRes(res);
    req.body.authType = 'local';
    req.body.authId = 'local_' + Date.now().toString();
    req.body.role = initRole(req.body.email);

    try {
        const emailExists = await User.findOne({ email: req.body.email });
        const usernameExists = await User.findOne({ username: req.body.username });

        const emailError = { message: 'Email already exists!' };
        const usernameError = { message: 'Username already exists!' };

        if (emailExists) return apiRes.sendError({ status: 400, message: emailError.message, errors: { email: emailError } });
        if (usernameExists) return apiRes.sendError({ status: 400, message: usernameError.message, errors: { username: usernameError } });

        const user = new User(req.body);
        await user.save();

        const {code} = await user.setEmailVerification();

        // if (code) await sendConfirmationEmail(code, req.body.email);

        return apiRes.sendSuccess({ data: user.toRegisteredUser() })
    } catch (error) {
        console.log(error)
        if (error.code === 'EAUTH') return apiRes.sendError({ message: 'Could not send mail!' })
        return apiRes.sendError({ error })
    }
}

async function resendEmail(req, res) {
    const apiRes = new ApiRes(res);

    try {
        const { email } = req.body;
        const user = await User.findOne({ email });
        const r = await user.setEmailVerification();
        if (!r.success) throw r.message;

        await sendConfirmationEmail(r.code, req.body.email);
        return apiRes.sendSuccess({ message: 'Check email for confirmation link' })
    } catch (error) {
        return apiRes.sendError({ error })
    }
}

async function resetPassword(req, res) {
    const apiRes = new ApiRes(res);
    const authType = 'local';
    const { email, code, password } = req.body;

    try {
        const user = await User.findOne({ email, authType })

        if(!user) return apiRes.sendNotFound();

        const r = await user.resetPassword(code, password);

        if(!r.success) return apiRes.sendError({ error: r })

        if (r.success) return apiRes.sendSuccess({ message : 'reset!'})
    } catch (error) {
        return apiRes.sendError({ error })
    }
}

async function recoverByEmail(req, res) {
    const apiRes = new ApiRes(res);
    const authType = 'local';
    const email = req.body.email;

    try {
        const user = await User.findOne({ email, authType })

        if(!user) return apiRes.sendNotFound({ message: 'The account does not exist!' });

        const r =await user.setPasswordReset();
        if (!r.success) throw r.message;

        await sendPasswordResetEmail(r.code, req.body.email);
        return apiRes.sendSuccess({ message: 'Check email for confirmation link' })
    } catch (error) {
        console.log(error)
        return apiRes.sendError({ error })
    }
}

module.exports = {
    registerUser,
    recoverByEmail,
    resendEmail,
    resetPassword
}