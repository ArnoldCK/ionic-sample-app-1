const superAdminEmail = process.env.SUPER_ADMIN_EMAIL || null;

module.exports = {
    initRole(email) {
        if (!superAdminEmail) return 'user';
        return email === superAdminEmail ? 'superAdmin' : 'user';
    }
}