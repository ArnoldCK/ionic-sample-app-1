const { ApiRes } = require('~core/base/utils');
const User = require('~core/auth/user/model');
const { sendConfirmationEmail } = require('~core/mailer/controller')

function getUser(req, res, next) {
    const apiRes = new ApiRes(res);
    const user = req.userPublic
    return apiRes.sendSuccess({ data: user });
}

async function verifyEmail(req, res, next) {
    const apiRes = new ApiRes(res);
    const code = req.params.code;

    try {
        const user = await User.findOne({ 'emailVerification.code': code });
        if (!user) return apiRes.sendError(res, { message : 'Invalid code!' });
        const r = await user.verifyEmail(code);
        if (!r.success) return apiRes.sendError({ error: r });
        return apiRes.sendSuccess({ message : `Email ${user.email} was verified!`});
    } catch(error) {
        console.log(error)
        apiRes.sendError({ error });
    }
}

async function reverifyEmail(req, res, next) {
    const apiRes = new ApiRes(res);
    const user = req.user;

    try {
        const r = await user.setEmailVerification();
        if (!r.success) return apiRes.sendError({ error: r });
        await sendConfirmationEmail(r.code, user.email);
        return apiRes.sendSuccess({ message : 'We sent a confirmation email. Please check your email!'})
    } catch(error) {
        apiRes.sendError({ error });
    }
}

async function logout(req, res, next) {
    const apiRes = new ApiRes(res);

    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token
        })

        await req.user.save()
        return apiRes.sendSuccess()
    } catch (error) {
        return apiRes.sendError({ error })
    }
}

async function logoutAll(req, res, next) {
    const apiRes = new ApiRes(res);
    try {
        req.user.tokens = new Array();
        await req.user.save()
        apiRes.sendSuccess()
    } catch (error) {
        return apiRes.sendError({ error })
    }
}

module.exports = {
    getUser,
    logout,
    logoutAll,
    verifyEmail,
    reverifyEmail
}