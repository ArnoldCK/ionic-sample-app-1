const { getToken } = require('./token');
const route = '/ws/';

module.exports = function(io) {
    const nsp = io.of(route);

    nsp.on('connection', async (socket) => {
        const token = await getToken(socket.handshake.query.token)
        const user = token ? token.user : null;
        console.log(`${user.name} connected to ${route}!`);
    })
}