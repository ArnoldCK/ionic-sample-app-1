const jwt = require('jsonwebtoken');
const opts = require('~config/opts');
const User = require('~core/auth/user/model');

async function getToken(token) {
    if (!token) return Promise.resolve(null);

    token = token.replace('Bearer ', '');

    if (!token.trim().length) return Promise.resolve(null);

    try {
        const decoded = jwt.verify(token, opts.passport.keys.jwt);
        const user = await User.findOne({ _id: decoded._id, 'tokens.token': token });
    
        if (user) {
            return Promise.resolve({ user: user, userPublic: user.toTokenUser(), token })
        }    
    } catch(e) {
        console.log(e)
    }
    
    Promise.resolve(null)
}

const useToken = async (req, res, next) => {
    try {
        let token = await getToken(req.header('Authorization'))
        if (!token) return next()

        req.user = token.user;
        req.userPublic = token.userPublic;
        req.token = token.token;
        next()
    } catch (e) {
        console.log(e)
        next()
    }
}

module.exports = {
    useToken,
    getToken
}
