const express = require('express');
const router = new express.Router();
const { memberOnly } = require('./guard');
const { logout, logoutAll, verifyEmail, reverifyEmail, getUser } = require('./controller');
const route = '/auth/';
require('./passport/local/router')(router);

/**
 * @swagger
 * definitions:
 *   NewUser:
 *     properties:
 *       username:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *   AuthUser:
 *     properties:
 *       _id:
 *         type: string
 *       username:
 *         type: string
 *       email:
 *         type: string
 *       emailVerified:
 *         type: boolean
 *       role:
 *         type: string      
 */

/**
 * @swagger
 * /auth/user:
 *    get:
 *      tags:
 *        - Auth
 *      security:
 *        - Bearer: []
 *      description: Get current api user
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: ''
 *          schema:
 *            type: object
 *            properties:
 *              success:
 *                type: boolean
 *              status:
 *                type: number
 *              data:
 *                type: object
 *                $ref: "#/definitions/AuthUser"
 */
router.get(`${route}user`, memberOnly, getUser);

/**
 * @swagger
 * /auth/verifyemail/{code}:
 *    get:
 *      tags:
 *        - Auth
 *      description: Verify email confirmation code
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: ''
 */
router.get(`${route}verifyemail/:code`, verifyEmail);

/**
 * @swagger
 * /auth/reverifyemail:
 *    get:
 *      tags:
 *        - Auth
 *      description: VResend email confirmation code
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: ''
 */
router.post(`${route}reverifyemail`, reverifyEmail);

/**
 * @swagger
 * /auth/logout:
 *    get:
 *      tags:
 *        - Auth
 *      security:
 *        - Bearer: []
 *      description: Logout from current device i.e clear current token
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: ''
 *        401:
 *          description: 'Token missing'
 */
router.get(`${route}logout`, memberOnly, logout);

/**
 * @swagger
 * /auth/logoutall:
 *    get:
 *      tags:
 *        - Auth
 *      security:
 *        - Bearer: []
 *      description: Logout on all devices i.e clears all tokens
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: ''
 *        401:
 *          description: 'Token missing'
 */
router.get(`${route}logoutall`, memberOnly, logoutAll);

module.exports = router;