const mongoose = require('mongoose');
const shortid = require('shortid');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const opts = require('~config/opts');
const { addVerification, VerificationType, checkVerification, resetVerification } = require('./types');
const _ = require('lodash');

const schema = mongoose.Schema({
    // Auth
    authType: {
        type: String,
        required: true
    },
    authId: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String
    },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        unique: true,
        required: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Email is invalid')
            }
        }
    },
    role: {
        type: String,
        enum : ['user','admin', 'superAdmin'],
        default: 'user'
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }],

    // Security
    locked: [{
        isActive: Boolean,
        isTimed: Boolean,
        reason: String,
        message: String,
        starts: Date,
        ends: Date,
        started: Date,
        ended: Date
    }],
    locked: {
        type: Boolean,
        default: false
    },
    lastActive: [{
        time: String,
        ip: String,
        action: String
    }],
    emailVerification: VerificationType,
    phoneVerification: VerificationType,
    otpVerification: VerificationType,
    passwordReset: VerificationType,

    // Profile
    photo: {
        type: String,
        trim: true
    },
    username: {
        type: String,
        trim: true,
        unique: true
    },
    displayName: {
        type: String,
        trim: true
    },
    firstName: {
        type: String,
        trim: true
    },
    lastName: {
        type: String,
        trim: true
    },

    // Balance
    balance: {
        type: Number,
        default: 0
    },
    balanceCurrency: {
        type: String,
        default: 'BTC'
    },
    withdrawalEnabled: {
        type: Boolean
    },

    // Game
    gamesPlayed: {
        type: Number,
        default: 0
    },
    gamesWon: {
        type: Number,
        default: 0
    },
})

// Static finders
schema.statics.findByEmail = async (email) => {
    const user = await User.findOne({ email })

    if (!user) {
        throw new Error('Not found')
    }

    return user
}

schema.statics.findByAuthId = async (authId) => {
    const user = await User.findOne({ authId })

    if (!user) {
        throw new Error('Unable to login')
    }

    return user
}

// Password reset
schema.methods.setPasswordReset = async function () {
    const user = this;
    const code = shortid.generate();
    const r = addVerification(user.passwordReset, code);
    await user.save();
    return r;
}

schema.methods.resetPassword = async function (code, password) {
    const user = this;
    const result = checkVerification(user.passwordReset, code);
    await user.save();
    if (!result.success) return result;

    user.password = password;
    user.tokens = new Array();
    resetVerification(user.passwordReset);
    await this.save();
    return { success: true };
}

// Email verification
schema.methods.setEmailVerification = async function () {
    const user = this;
    const code = shortid.generate();
    const r = addVerification(user.emailVerification, code, 'public');
    await user.save();
    return r;
}

schema.methods.verifyEmail = async function (code) {
    const user = this;
    const result = checkVerification(user.emailVerification, code);
    await user.save();
    return result;
}

// Filters
schema.methods.toTokenUser = function() {
    const user = this.toObject();
    let filtered = _.pick(user, ['_id', 'email', 'username', 'role']);
    filtered.emailVerified = user.emailVerification.passed;
    return filtered;
}

schema.methods.toPublicUser = function() {
    const user = this.toObject();
    return _.pick(user, ['_id', 'email', 'emailVerification', 'locked', 'username', 'role', 'photo']);
}

schema.methods.toRegisteredUser = function() {
    const user = this.toObject();
    return _.pick(user, ['email', 'username', 'role']);
}

// Login
schema.methods.setToken = async function () {
    const user = this;
    const _user = this.toTokenUser();
    const token = jwt.sign(_user, opts.passport.keys.jwt, opts.passport.jwt);

    user.tokens.push({ token });

    await user.save();

    return token;
}

// Login
schema.methods.authenticate = function(password) {
    return bcrypt.compareSync(password, this.password)
}

// Save
schema.pre('save', async function (next) {
    if (this.isModified('password')) {
        this.password = await bcrypt.hash(this.password, 8)
    }

    next()
})

const User = mongoose.model('User', schema)
module.exports = User
