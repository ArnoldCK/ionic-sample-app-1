const moment = require('moment');
const opts = require('~config/opts');

const VerificationType = {
    authType: {
        type: String,
        enum : ['public', 'private'],
        default: 'private'
    },
    locked: Boolean,
    lockedExpiry: Date,
    resetCount: Number,
    passDate: Date,
    passed: Boolean,
    code: String,
    codeDate: Date,
    codeExpiry: Date,
    verifications: [{
        code: String,
        codeDate: Date,
        codeExpiry: Date,
        passDate: Date,
        passed: Boolean,
        expired: Boolean,
        maxedOut: Boolean,
        tries: [{
            userCode: String,
            userCodeDate: Date,
            passed: Boolean,
            expired: Boolean,
            browser: String,
            ip: String
        }]
    }]    
}

function resetVerification(field, clearVerifications = false) {
    field.locked = false;
    field.lockedExpiry = null;
    field.resetCount = 0;
    field.passDate = null;
    field.passed = false;
    field.code = null;
    field.codeDate = null;
    field.codeExpiry = null;

    if (clearVerifications) field.verifications = new Array();
}

function addVerification(field, code, type = 'private') {
    // Return if passed
    if (field.passed) {
        return { success: false, message: 'Already verified!'}
    }

    const now = Date.now();
    const expiry = new Date(field.lockedExpiry);

    // Return if locked
    if (field.locked && now < expiry.getTime()) {
        const d = moment(field.lockedExpiry).fromNow();
        return { success: false, message: `Locked after max ${field.resetCount} attempts! Try again ${d}`};
    }

    // Reset if lock expires
    if (field.locked && now > expiry.getTime()) {
        field.resetCount = 0;
    }

    // Return if max passed
    if (field.resetCount >= opts.verification.maxResets) {
        field.locked = true;
        field.lockedExpiry = now + opts.verification.lockedExpiry;
        const d = moment(field.lockedExpiry).fromNow();
        return { success: false, message: `Locked after ${field.resetCount} attempts! Try again ${d}`};
    }

    // New verification
    field.authType = type;
    field.locked = false;
    field.lockedExpiry = null;
    field.resetCount = isNaN(field.resetCount) ? 0 : field.resetCount + 1;
    field.passDate = null;
    field.passed = false;
    field.code = code;
    field.codeDate = now;
    field.codeExpiry = now + opts.verification.trialExpiry;

    const verification = {
        code: code,
        codeDate: now,
        codeExpiry: now + opts.verification.trialExpiry,
        passDate: null,
        passed: false,
        expired: false,
        maxedOut: false,
        tries: []
    };

    if (!field.verifications) field.verifications = [];

    field.verifications.unshift({
        code: code,
        codeDate: now,
        codeExpiry: now + opts.verification.trialExpiry,
        passDate: null,
        passed: false,
        expired: false,
        maxedOut: false,
        tries: []
    })

    return { success: true, code }
}

function checkVerification(field, code, max = opts.verification.maxTries) {

    if (!field || !field.verifications || !field.verifications.length) return { success: false, message: 'Request a verification code first!'}
    if (field.passed) return { success: false, message: 'Already verified!'}

    const now = Date.now();
    const verification = field.verifications[0];
    const expiry = new Date(verification.codeExpiry);

    // Return if max tries
    if (verification.tries.length >= max) {
        verification.maxedOut = true;
        return { success: false, message: 'Too many attempts! Request new code.'}
    }

    // Proceed with new trial
    verification.tries.unshift({
        userCode: code,
        userCodeDate: now,
        passed: false,
        expired: false,
        browser: '',
        ip: ''
    })
    const trial = verification.tries[0];

    // If expired return
    if (now > expiry.getTime()) {
        trial.passed = false;
        trial.expired = true;
        verification.passed = false;
        return { success: false, message: 'Code expired! Request new code'}
    }

    // Failed
    if (trial.userCode !== verification.code) {
        trial.passed = false;
        verification.passed = false;
        return { success: false, message: 'Invalid code!'}
    }

    // Passed
    trial.passed = true;
    verification.passed = true;
    verification.passDate = now;
    field.passed = true;
    field.passDate = now;

    return { success: true }
}

module.exports = {
    VerificationType,
    checkVerification,
    addVerification,
    resetVerification
}