const { ApiRes } = require('~core/base/utils');

function allowRoles(roles = ['user'], req, res, next) {
    const apiRes = new ApiRes(res);
    if (!req.user) return apiRes.sendExpired();
    if (!roles.some(r => r === req.user.role)) return apiRes.sendDenied();
    return next()
}

function memberOnly(req, res, next) {
    return allowRoles(['user'], req, res, next)
}

function adminOnly(req, res, next) {
    return allowRoles(['admin'], req, res, next)
}

function superAdminOnly(req, res, next) {
    return allowRoles(['superAdmin'], req, res, next)
}

function staffOnly(req, res, next) {
    return allowRoles(['staff', 'admin', 'superAdmin'], req, res, next)
}

function moderatorOnly(req, res, next) {
    return allowRoles(['staff', 'admin', 'superAdmin', 'moderator'], req, res, next)
}

module.exports = {
    memberOnly,
    adminOnly,
    superAdminOnly,
    staffOnly,
    moderatorOnly
}
