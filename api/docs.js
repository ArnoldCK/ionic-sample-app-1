import swaggerJsdoc from 'swagger-jsdoc';

const options = {
    swaggerDefinition: {
        info: {
            title: 'Ionic Sample App API',
            version: '1.0.0',
            description: ''
        },
        servers: [
            {
                url: 'http://localhost:4000/'
            }
        ],
        securityDefinitions: {
            Bearer: {
                type: 'apiKey',
                name: 'Authorization',
                in: 'header'
            }
        }
    },
    apis: [
        './core/base/router.js',
        './core/events/router.js', 
        './core/users/router.js',
        './core/auth/router.js',
        './core/auth/passport/local/router.js'
    ]
};

export const docs = swaggerJsdoc(options);
module.exports = docs
