import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AuthService, LoginUser } from '../../providers/auth/';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user: LoginUser = { email: '', password: '' };
  submitted = false;
  busy = false;
  success = false;
  error = null;
  auth = null;

  constructor(
    private authService: AuthService,
    private router: Router,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController ) {
  }

  ngOnInit() {
  }

  async onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.busy = false;
      const loading = await this.loadingCtrl.create({ message: 'Authenticating...' });
      await loading.present();
      try {
        const r = await this.authService.login(this.user);
        this.success = true;
        console.log(r);
        this.router.navigateByUrl('/');
      } catch ({ error }) {
        console.log(error);
        this.error = error;
        const message = error.message || error.error && error.error.message || 'Unknown Error!';

        const alert = await this.alertCtrl.create({ message });
        await alert.present();
      }
      loading.dismiss();
    }
  }
}
