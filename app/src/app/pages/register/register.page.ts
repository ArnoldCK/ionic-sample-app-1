import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService, NewUser } from '../../providers/auth/';
import { LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  user: NewUser = {
    username: '',
    email: '',
    password: ''
  };

  submitted = false;
  busy = false;
  success = false;
  error = null;

  constructor(
    private authService: AuthService,
    private router: Router,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController ) { }

  ngOnInit() {
  }

  async onRegister(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.busy = false;
      const loading = await this.loadingCtrl.create({ message: 'Creating account...' });
      await loading.present();

      try {
        const r = await this.authService.register(this.user).toPromise();
        this.success = true;
        console.log(r);
        this.router.navigateByUrl('/login');
      } catch ({ error }) {
        console.log(error);
        this.error = error;
        const message = error.message || error.error && error.error.message || 'Unknown Error!';

        const alert = await this.alertCtrl.create({ message });
        await alert.present();
      }

      loading.dismiss();
    }
  }

}
