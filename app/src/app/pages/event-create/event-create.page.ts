import { Component, OnInit, OnDestroy } from '@angular/core';
import { Event, EventsService } from '../../providers/events/';
import { UsersService, DummyUser } from '../../providers/users';
import { NgForm } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-event-create',
  templateUrl: './event-create.page.html',
  styleUrls: ['./event-create.page.scss'],
})
export class EventCreatePage implements OnInit, OnDestroy {
  event: Event = { comment: '', time: '', userId: ''};
  submitted = false;
  busy = false;
  success = false;
  error = null;
  users: DummyUser[] = [];
  usersSub = null;

  constructor(
    private eventsService: EventsService,
    private usersService: UsersService,
    private router: Router,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController ) {
    }

  ngOnInit() {
    this.usersSub = this.usersService.users.subscribe(users => {
      this.users = users;
    });
  }

  ngOnDestroy() {
    this.usersSub.unsubscribe();
  }

  async onSubmit(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.busy = false;
      const loading = await this.loadingCtrl.create({ message: 'Submitting...' });
      await loading.present();
      try {
        const r = await this.eventsService.addEvent(this.event);
        this.success = true;
        console.log(r);
        this.router.navigateByUrl('/events');
      } catch ({ error }) {
        console.log(error);
        const message = error.message || error.error && error.error.message || 'Unknown Error!';
        const alert = await this.alertCtrl.create({ message });
        await alert.present();
      }

      loading.dismiss();
    }
  }
}
