import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { EventsService } from '../../providers/events';
import { UsersService, DummyUser } from '../../providers/users';
import { DisplayEvent } from '../../providers/events/events.model';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-events',
  templateUrl: 'events.page.html',
  styleUrls: ['events.page.scss'],
})
export class EventsPage implements OnInit, OnDestroy {
  events: DisplayEvent[] = [];
  users: DummyUser[] = [];
  eventsSub = null;

  constructor(
  private router: Router,
  private alertCtrl: AlertController,
  private eventsService: EventsService,
  private usersService: UsersService) {
  }

  ngOnInit() {
    this.eventsSub = this.eventsService.events.subscribe(events => {
      this.events = this.eventsService.toDisplayEvents(events, this.users);
    });

    this.getEvents();
  }

  ngOnDestroy() {
    this.eventsSub.unsubscribe();
  }

  async getEvents() {
    try {
      this.users = await this.usersService.getLoadedUsers();
      await this.eventsService.getAll();
    } catch (error) {
      const message = error.message || error.error && error.error.message || 'Unknown Error!';
      const alert = await this.alertCtrl.create({ message });
      await alert.present();
    }
  }
}
