import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../providers/auth/auth.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit, OnDestroy {
  user = null;
  authSub = null;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.authSub = this.authService.user.subscribe(user => {
      this.user = user;
    });
  }

  ngOnDestroy() {
    this.authSub.unsubscribe();
  }
}
