import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Event, EventsService, DisplayEvent } from '../../providers/events/';
import { UsersService } from '../../providers/users';
import { AuthService } from '../../providers/auth';
import { LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.page.html',
  styleUrls: ['./event-detail.page.scss'],
})
export class EventDetailPage implements OnInit, OnDestroy {
  event: DisplayEvent = null;
  busy = false;
  users = [];
  loggedIn = false;
  routeSub = null;
  authSub = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private usersService: UsersService,
    private eventService: EventsService,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private router: Router) {
  }

  ngOnInit() {
    this.authSub = this.authService.loggedIn.subscribe(loggedIn => {
      this.loggedIn = loggedIn;
    });

    this.routeSub = this.activatedRoute.paramMap.subscribe(paramMap => {
      const eventId = paramMap.get('eventId');
      if (!eventId) {
        return;
      }

      this.getEvent(eventId);
    });
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
    this.authSub.unsubscribe();
  }

  async getEvent(eventId: string) {
    this.busy = true;
    try {
      const users = await this.usersService.getLoadedUsers();
      const r: any = await this.eventService.getEvent(eventId).toPromise();
      this.event = this.eventService.toDisplayEvent(r.data, users);
      console.log(this.event);
    } catch (error) {
      console.log(error);
    }
    this.busy = false;
  }

  async onDelEvent(eventId: string) {
    const alert = await this.alertCtrl.create({
      header: 'Confirm!',
      message: 'Are you sure you want to delete?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            return;
          }
        }, {
          text: 'Okay',
          handler: () => {
            this.delEvent(eventId);
          }
        }
      ]
    });

    await alert.present();
  }
  
  async delEvent(eventId: string) {
    const loading = await this.loadingCtrl.create({ message: 'working...' });
    loading.present();

    try {
      await this.eventService.delEvent(eventId);
      this.event = null;
      this.router.navigateByUrl('/events');
    } catch (error) {
      const alert = await this.alertCtrl.create({ message: error.error.message });
      await alert.present();
      console.log(error.error.message);
    }
    loading.dismiss();
  }
}
