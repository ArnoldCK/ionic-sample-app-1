import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Event, EventsService } from '../../providers/events/';
import { UsersService, DummyUser } from '../../providers/users';
import { NgForm } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-event-update',
  templateUrl: './event-update.page.html',
  styleUrls: ['./event-update.page.scss'],
})
export class EventUpdatePage implements OnInit, OnDestroy {
  event: Event = { _id: '', comment: '', time: '', userId: ''};
  submitted = false;
  busy = false;
  success = false;
  error = null;
  users: DummyUser[] = [];
  routeSub = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private eventsService: EventsService,
    private usersService: UsersService,
    private router: Router,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController ) {
    }

  ngOnInit() {
    this.routeSub = this.activatedRoute.paramMap.subscribe(paramMap => {
      const eventId = paramMap.get('eventId');
      if (!eventId) {
        return;
      }

      this.getEvent(eventId);
    });
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }

  async getEvent(eventId: string) {
    this.busy = true;
    try {
      this.users = await this.usersService.getLoadedUsers();
      const r: any = await this.eventsService.getEvent(eventId).toPromise();
      this.event = r.data;
      console.log(this.event);
    } catch (error) {
      console.log(error);
    }
    this.busy = false;
  }

  async onSubmit(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.busy = false;
      const loading = await this.loadingCtrl.create({ message: 'Submitting...' });
      await loading.present();
      try {
        const r = await this.eventsService.updateEvent(this.event);
        this.success = true;
        console.log(r);
        loading.dismiss();
        this.router.navigateByUrl('/events');
      } catch ({ error }) {
        loading.dismiss();
        this.error = error.error;
        const alert = await this.alertCtrl.create({ message: this.error.message });
        await alert.present();
        console.log(this.error.message);
      }
    }
  }
}
