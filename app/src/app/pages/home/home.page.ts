import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../providers/auth'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  loggedIn = false;

  constructor(private authService: AuthService) {
    this.authService.loggedIn.subscribe(v => {
      this.loggedIn = v;
    });
  }
}
