import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { DummyUser } from './users.model';
import { Plugins } from '@capacitor/core';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
    private _users = new BehaviorSubject<DummyUser[]>([]);

    constructor(private http: HttpClient) {}

    get users() {
        return this._users.asObservable();
    }

    async getLoadedUsers() {
        const data = await Plugins.Storage.get({ key: 'USERS' });
        const users = data && data.value ? JSON.parse(data.value) : [];
        return Promise.resolve(users);
    }

    private storeUsers(data) {
        Plugins.Storage.set({ key: 'USERS', value: JSON.stringify(data)});
    }

    async getAll() {
        return new Promise(async (resolve, reject) => {

            const data = await Plugins.Storage.get({ key: 'USERS' });

            if (data && data.value) {
                const users = JSON.parse(data.value);
                this.setUsers(users);
                resolve(users);
            }

            this.http.get('users').toPromise()
              .then((r: { data: DummyUser[] }) => {
                this.setUsers(r.data);
                this.storeUsers(r.data);
                resolve(r.data);
              })
              .catch(error => {
                console.log(error);
                if (!error.error) {
                  return reject({ error: { message: error.message }});
                }
                reject(error);
              });
          });
    }

    setUsers(data: DummyUser[]) {
        this._users.next(data);
    }
}
