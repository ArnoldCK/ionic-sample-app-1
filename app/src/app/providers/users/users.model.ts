export interface DummyUser {
    id: string;
    firstName: string;
    lastName: string;
}