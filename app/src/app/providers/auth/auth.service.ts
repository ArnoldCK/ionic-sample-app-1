import { Injectable } from '@angular/core';
import { User, LoginUser, NewUser } from './auth.model';
import { Observable, BehaviorSubject, from, of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import jwtDecode from 'jwt-decode';
import { Plugins } from '@capacitor/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _user = new BehaviorSubject<User>(null);
  token = null;

  constructor(private http: HttpClient) {
    this.initUser();
  }

  get user(): Observable<User> {
    return this._user.asObservable();
  }

  get loggedIn(): Observable<boolean> {
    return this.user.pipe(map(user => user ? true : false));
  }

  logout() {
    this._user.next(null);
    Plugins.Storage.remove({ key: 'AUTH' });
  }

  login(user: LoginUser) {
    console.log(user);
    return new Promise((resolve, reject) => {
      this.http.post('auth/local/login', user).toPromise()
        .then((r: { data: any }) => {
          const decoded = jwtDecode(r.data);
          decoded._token = r.data;
          this.setUser(decoded);

          resolve(r.data);
        })
        .catch(error => {
          console.log(error);
          if (!error.error) {
            return reject({ error: { message: error.message }});
          }
          reject(error);
        });
    });
  }

  register(user: NewUser): Observable<any> {
    console.log(user);
    return this.http.post('auth/local/register', user);
  }

  async initUser() {
    const data = await Plugins.Storage.get({ key: 'AUTH' });
    console.log(data)
    if (data && data.value) {
      this.setUser(JSON.parse(data.value));
    }
  }

  private storeUser(data) {
    Plugins.Storage.set({ key: 'AUTH', value: JSON.stringify(data)});
  }

  setUser(data: User) {
    console.log(data);
    const { _id, _token, email, emailVerified, exp, iat, role, username } = data;
    this._user.next(new User(
      _id,
      _token,
      email,
      emailVerified,
      exp,
      iat,
      role,
      username
    ));
    this.token = _token;
    this.storeUser(data);
  }
}
