import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanLoad {
    constructor(private authService: AuthService, private router: Router) {}

    canLoad(
        route: Route,
        segments: UrlSegment[]
    ): Observable<boolean> | Promise<boolean> | boolean {
        this.authService.loggedIn.subscribe(loggedIn => {
            console.log('route auth', loggedIn)
            if (!loggedIn) {
                this.router.navigateByUrl('/denied');
            }
        });

        return this.authService.loggedIn.pipe(take(1));
    }
}
