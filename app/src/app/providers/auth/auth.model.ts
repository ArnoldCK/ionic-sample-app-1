export class User {
    constructor(
        public _id: string, public _token: string,
        public email: string,
        public emailVerified: boolean,
        public exp: number,
        public iat: number,
        public role: string,
        public username: string,
    ) {}

    get token() {
        if (this.exp <= Date.now()) {
            return null;
        }

        return this._token;
    }
}

export class LoginUser {
    constructor(
        public email: string,
        public password: string
    ) {}
}

export class NewUser {
    constructor(
        public username: string,
        public email: string,
        public password: string
    ) {}
}
