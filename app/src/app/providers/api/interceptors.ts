import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

    constructor(public auth: AuthService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const baseUrl = 'http://localhost:4000/';
        const apiReq = req.clone({
            url: `${baseUrl}${req.url}`,
            setHeaders: {
                Authorization: this.auth.token ? `Bearer ${this.auth.token}` : ''
            }
        });

        return next.handle(apiReq).pipe(
            map(event => event),
            catchError((error: HttpErrorResponse) => {
                if (error.status === 401) {
                    this.auth.logout();
                }

                return throwError(error);
            })
        );
    }
}

export const ApiProviders = [
    { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true },
];
