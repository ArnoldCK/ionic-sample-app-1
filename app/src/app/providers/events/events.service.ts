import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { Event, DisplayEvent } from './events.model';
import { DummyUser, UsersService } from '../users/';
import _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class EventsService {
    private _events = new BehaviorSubject<Event[]>([]);
    private users: DummyUser[] = [];

    constructor(private http: HttpClient, private usersService: UsersService) {

    }

    get events() {
        return this._events.asObservable();
    }

    async loadUsers() {
      try {
        const data: any = this.usersService.getAll();
        this.users = data;
      } catch(error) {
        console.log(error);
      }
    }

    getAll() {
        return new Promise((resolve, reject) => {
            this.http.get('events').toPromise()
              .then((r: { data: any }) => {
                this.setEvents(r.data);
                console.log(r.data);
                resolve(r.data);
              })
              .catch(error => {
                console.log(error)
                reject(error)
              });
          });
    }

    getEvent(eventId: string) {
      return this.http.get(`events/${eventId}`);
    }

    delEvent(eventId: string) {
      return new Promise((resolve, reject) => {
        this.http.delete(`events/${eventId}`).toPromise()
          .then((r: { data: any }) => {
            this.removeEvent(r.data._id);
            console.log(r.data)
            resolve(r.data);
          })
          .catch(error => {
            console.log(error)
            reject(error)
          });
      });
    }

    addEvent(event: Event) {
        return new Promise((resolve, reject) => {
            this.http.post('events', event).toPromise()
              .then((r: { data: any }) => {
                this.appendEvent(r.data);
                console.log(r.data);
                resolve(r.data);
              })
              .catch(error => {
                console.log(error)
                reject(error)
              });
          });
    }

    updateEvent(event: Event) {
      return new Promise((resolve, reject) => {
          this.http.put('events', event).toPromise()
            .then((r: { data: any }) => {
              this.replaceEvent(r.data);
              console.log(r.data);
              resolve(r.data);
            })
            .catch(error => {
              console.log(error)
              reject(error)
            });
        });
  }

    setEvents(data: Event[]) {
        this._events.next(data);
    }

    appendEvent(event: Event) {
        const events = [event, ...this._events.value];
        this.setEvents(events);
    }

    removeEvent(eventId: string) {
      const events = this._events.value.filter(e => e._id !== eventId);
      this.setEvents(events);
    }

    replaceEvent(event: Event) {
      const events = [...this._events.value];
      const i = events.findIndex(e => e._id === event._id);
      events[i] = event;
      this.setEvents(events);
    }

    toDisplayEvents(events: Event[], users: DummyUser[]): DisplayEvent[] {
      if (!events || !events.length) {
        return [];
      }
      return events.map(e => {
        const user = users.find(u => u.id === e.userId);
        return { user, ...e};
      });
    }

    toDisplayEvent(event: Event, users: DummyUser[]): DisplayEvent {
        const user = users.find(u => u.id === event.userId);
        return { user, ...event};
    }
}
