import { DummyUser } from '../users/';

export class Event {
    constructor(
        public comment: string,
        public time: string,
        public userId: string,
        public _id?: string
    ) {}
}

export class DisplayEvent {
    constructor(
        public comment: string,
        public time: string,
        public userId: string,
        public user: DummyUser,
        public _id?: string
    ) {}
}
