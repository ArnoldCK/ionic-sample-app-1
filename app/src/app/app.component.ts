import { Component, OnInit, OnDestroy } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './providers/auth';
import { UsersService } from './providers/users';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  public appPages = [
    {
      title: 'Home',
      url: '/',
      icon: 'home'
    },
    {
      title: 'Events',
      url: '/events',
      icon: 'list'
    }
  ];

  loggedIn = false;
  authSub = null;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService,
    private usersService: UsersService,
    private loadingCtrl: LoadingController,
    private router: Router
  ) {
    this.initializeApp();
  }

  ngOnInit() {
    this.authSub = this.authService.loggedIn.subscribe(loggedIn => {
      this.loggedIn = loggedIn;
    });

    this.loadData();
  }

  ngOnDestroy() {
    this.authSub.unsubscribe();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  async loadData() {
    const loading = await this.loadingCtrl.create({ message: 'Loading...' });
    await loading.present();
    try {
      await this.usersService.getAll();
    } catch (error) {
      console.log(error);
    }

    loading.dismiss();
  }

  logout() {
    this.authService.logout();
    this.router.navigateByUrl('/');
  }
}
