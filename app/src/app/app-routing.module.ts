import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './providers/auth/';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'events',
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/events/events.module').then(m => m.EventsPageModule)
      },
      {
        path: 'detail/:eventId',
        loadChildren: './pages/event-detail/event-detail.module#EventDetailPageModule'
      },
      {
        path: 'edit/:eventId',
        loadChildren: './pages/event-update/event-update.module#EventUpdatePageModule',
        canLoad: [AuthGuard]
      }
    ]
  },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'account', loadChildren: './pages/account/account.module#AccountPageModule', canLoad: [AuthGuard] },
  { path: 'events/create', loadChildren: './pages/event-create/event-create.module#EventCreatePageModule', canLoad: [AuthGuard] },
  { path: 'denied', loadChildren: './pages/denied/denied.module#DeniedPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
