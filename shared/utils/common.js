function isUndef(v) {
    return v === undefined || v === null;
}

function isDef(v) {
    return v !== undefined && v !== null;
}

function isEmpty(v) {
    if (isUndef(v)) return true;
    if (isObject(v)) return Object.keys(v).length === 0;
    return v.length === 0;
}

function isObject(v) {
    if (isUndef(v)) return false;
    return Object.prototype.toString.call(v) === '[object Object]'
}

function isArray(v) {
    if (isUndef(v)) return false;
    return Object.prototype.toString.call(v) === '[object Array]'
}

function isString(v) {
    if (isUndef(v)) return false;
    return Object.prototype.toString.call(v) === '[object String]'
}

function isNotEmpty(v) {
    return !isEmpty(v);
}

function enumToArray(v) {
    return Object.keys(v).map(k => v[k]);
}

function joinObj(obj, glue, separator) {
    if (!obj) throw new Error('joinObj(): No object provided!');

    return Object.keys(obj).map(k => {
        return `${k}${glue}${obj[k]}`
    }).join(separator)
}

function querify(query) {
    if (query.length === 0) return '';
    return joinObj(query, '=', '&');
}

function modelQuery(keys, query) {
    const q = {}

    keys.forEach(k => {
        const v = query[k];
        if (v) q[k] = v
    })

    return q;
}

function isInteger(v) {
    if (isNaN(v)) return false;
    return Math.floor(v) === v
}

function isPositiveInt(v) {
    return isInteger(v) && v > 0
}

function randomInt(len = 6) {
    const a = ('8' + Array(len - 1).fill(9).join('')) * 1;
    const b = ('1' + Array(len - 1).fill(0).join('')) * 1;
    return Math.floor(Math.random() * a + b);
}

function levelObject(obj) {
    Object.keys(obj).forEach(k => obj[k] = k)
}

function levelObjects(objs = []) {
    objs.forEach(obj => levelObject(obj))
}

function hmsToSecs(str = '0s') {
    const d = str.match(/(\d+)d/);
    const h = str.match(/(\d+)h/);
    const m = str.match(/(\d+)m/);
    const s = str.match(/(\d+)s/);
    const t = [d? d[1] : 0, h? h[1] : 0, m? m[1] : 0, s? s[1] : 0];
    return t.reverse().reduce((prev, curr, i) => {
        if (i === 3) return prev + curr*24*Math.pow(60, 2)
        return prev + curr*Math.pow(60, i)
    }, 0)
}

function hmsToMsecs(str) {
    return hmsToSecs(str) * 1000
}

function mapErrors(model, errors) {
    const result = { ...model };

    for (const k in result) {
        result[k] = null;
        const error = isDef(errors[k]) ? errors[k] : null;
        if (errors) result[k] = error ? { ...error, pass: false } : { pass: null };
    }

    return result;
}

function cloneObj(orig) {
    return Object.assign(Object.create(orig), orig)
}

function dialBack(length, index, steps) {
    const i = index - (steps % length);
    return i < 0 ? length + i : i
}

function dialForward(length, index, steps) {
    const i = (index + steps) % length;
    return i
}

module.exports = {
    enumToArray,
    isArray,
    isDef,
    isEmpty,
    isInteger,
    isNotEmpty,
    isString,
    isObject,
    isPositiveInt,
    isUndef,
    joinObj,
    levelObject,
    levelObjects,
    modelQuery,
    querify,
    randomInt,
    hmsToSecs,
    hmsToMsecs,
    mapErrors,
    cloneObj,
    dialBack,
    dialForward
}
