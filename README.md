# ionic-sample-app-1

## Api Setup
cd to /api
``` bash
# install dependencies
$ npm install

# rename env.sample to .env

# serve with hot reload at localhost:4000
$ npm run dev

# view swagger generated API docs at localhost:4000/docs

```

## Ionic app Setup
cd to /app
``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:4200
$ npm start

# set chrome browsing device to iphone or google pixel to view app
```
